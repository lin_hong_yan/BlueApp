/*
 Navicat Premium Data Transfer

 Source Server         : OA行政服务-测试库(192.168.240.205_8066)
 Source Server Type    : MySQL
 Source Server Version : 50629
 Source Host           : 192.168.240.205:8066
 Source Schema         : office_auto

 Target Server Type    : MySQL
 Target Server Version : 50629
 File Encoding         : 65001

 Date: 20/03/2020 14:48:15
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for oa_remake_seal_apply_approver
-- ----------------------------
-- DROP TABLE IF EXISTS `oa_remake_seal_apply_approver`;
CREATE TABLE `oa_remake_seal_apply_approver`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `apply_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '申请主键',
  `node_id` int(10) UNSIGNED NOT NULL COMMENT '节点主键',
  `approve_node` tinyint(2) UNSIGNED NOT NULL COMMENT '审核节点（1审核，2会审，3审批）',
  `approve_index` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '版本序号：记录当前是第几次提交（未提交过则为0）',
  `deletable_flag` tinyint(1) NULL DEFAULT 1 COMMENT '可删除标记（0-不可删除，1-可删除）',
  `emp_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '审批人',
  `emp_code` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '审批人工号',
  `emp_department` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '审批人部门',
  `emp_dept_code` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '审批人部门编号',
  `emp_position` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '审批人职位',
  `approve_flag` tinyint(1) NULL DEFAULT 0 COMMENT '审核标记（0未审核，1已审核）',
  `approve_opinion` tinyint(2) NULL DEFAULT NULL COMMENT '审核意见（0不通过，1通过）',
  `approve_remark` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '审核说明',
  `approve_time` datetime(0) NULL DEFAULT NULL COMMENT '审核时间',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建人',
  `create_code` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '创建人工号',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间、删除时间',
  `update_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新人、删除人',
  `update_code` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '更新人工号、删除人工号',
  `deleted_flag` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '删除标记（0未删除，1已删除）',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_apply_id`(`apply_id`) USING BTREE,
  INDEX `index_approve_node`(`approve_node`) USING BTREE,
  INDEX `index_emp_code`(`emp_code`) USING BTREE,
  INDEX `index_approve_time`(`approve_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 273 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '印章使用申请审批人及其审批意见' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for oa_remake_seal_apply_details
-- ----------------------------
-- DROP TABLE IF EXISTS `oa_remake_seal_apply_details`;
CREATE TABLE `oa_remake_seal_apply_details`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `apply_id` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '申请主键',
  `file_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '文件名称',
  `seal_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '印章编号',
  `seal_company_code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '印章所属公司编码',
  `seal_company_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '印章所属公司名称',
  `seal_type` tinyint(2) UNSIGNED NULL DEFAULT 0 COMMENT '印章类别（字典：oa_seal_type）',
  `seal_use_rule` tinyint(2) UNSIGNED NULL DEFAULT 1 COMMENT '盖章规则（字典：oa_seal_use_rule）',
  `file_count` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '文件份数',
  `file_page_count` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '每份页数',
  `seal_count` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '每份落款章个数',
  `seal_total` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '合计落款章个数',
  `seal_on_perforation_flag` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '加盖骑缝章标记（0不加骑缝章，1加骑缝章）',
  `seal_on_perforation_count` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '每份骑缝章个数',
  `seal_on_perforation_total` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '合计骑缝章个数',
  `stamp_status` tinyint(2) UNSIGNED NULL DEFAULT 1 COMMENT '盖章状态（字典：oa_remake_seal_stamp_status）',
  `stamped_count` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '已盖章份数',
  `watermark_flag` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '是否带水印（0不带水印，1带水印）',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建人',
  `create_code` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '创建人工号',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间、删除时间',
  `update_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新人、删除人',
  `update_code` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '更新人工号、删除人工号',
  `deleted_flag` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '删除标记（0未删除，1已删除）',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_apply_id`(`apply_id`) USING BTREE,
  INDEX `index_seal_id`(`seal_id`) USING BTREE,
  INDEX `index_seal_company_code`(`seal_company_code`) USING BTREE,
  INDEX `index_stamp_status`(`stamp_status`) USING BTREE,
  INDEX `index_create_code`(`create_code`) USING BTREE,
  INDEX `index_update_time`(`update_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 178 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '印章使用申请的用印信息' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for oa_remake_seal_apply_file
-- ----------------------------
-- DROP TABLE IF EXISTS `oa_remake_seal_apply_file`;
CREATE TABLE `oa_remake_seal_apply_file`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `apply_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '申请主键',
  `file_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '文件名称',
  `file_size` decimal(13, 2) UNSIGNED NULL DEFAULT 0.00 COMMENT '文件大小（KB）',
  `file_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '文件类型（文件名后缀）',
  `file_url` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '文件存储地址',
  `watermark_flag` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '是否嵌入水印',
  `watermark_file_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '带水印文件名称',
  `watermark_file_size` decimal(13, 2) UNSIGNED NULL DEFAULT 0.00 COMMENT '带水印文件大小',
  `watermark_file_url` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '带水印文件地址',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建人',
  `create_code` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '创建人工号',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间、删除时间',
  `update_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新人、删除人',
  `update_code` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '更新人工号、删除人工号',
  `deleted_flag` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '删除标记（0未删除，1已删除）',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_apply_id`(`apply_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 197 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '印章申请附件及其带水印文件' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for oa_remake_seal_apply_node
-- ----------------------------
-- DROP TABLE IF EXISTS `oa_remake_seal_apply_node`;
CREATE TABLE `oa_remake_seal_apply_node`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `apply_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '申请主键',
  `approve_node` tinyint(2) UNSIGNED NOT NULL COMMENT '审核节点（1审核，2会审，3审批）',
  `approve_index` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '版本序号：记录当前是第几次提交（未提交过则为0）',
  `approver_total` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '审核人总数',
  `approved_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '已审核人数',
  `pass_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '通过人数',
  `refuse_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '不通过人数',
  `approve_result` tinyint(2) UNSIGNED NULL DEFAULT 0 COMMENT '审核结果（0未审核，1审核通过，2审核不通过，3审核中）',
  `approve_start_time` datetime(0) NULL DEFAULT NULL COMMENT '审核开始时间（上一个节点结束，则本节点开始）',
  `approve_end_time` datetime(0) NULL DEFAULT NULL COMMENT '审核结束时间',
  `start_way` tinyint(2) UNSIGNED NOT NULL DEFAULT 1 COMMENT '开始方式（1正常流入，2继续流转）',
  `handle_way` tinyint(2) UNSIGNED NOT NULL DEFAULT 1 COMMENT '处理方式（1默认值-无需处理，2待处理，3继续流转，4重新提交, 5已撤销）',
  `handle_remark` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '处理说明（流转说明）',
  `handle_time` datetime(0) NULL DEFAULT NULL COMMENT '处理时间',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建人',
  `create_code` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '创建人工号',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间、删除时间',
  `update_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新人、删除人',
  `update_code` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '更新人工号、删除人工号',
  `deleted_flag` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '删除标记（0未删除，1已删除）',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_apply_id`(`apply_id`) USING BTREE,
  INDEX `index_approve_node`(`approve_node`) USING BTREE,
  INDEX `index_handle_way`(`handle_way`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 266 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '印章使用申请审批节点及其流转记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for oa_remake_seal_apply_record
-- ----------------------------
-- DROP TABLE IF EXISTS `oa_remake_seal_apply_record`;
CREATE TABLE `oa_remake_seal_apply_record`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `apply_no` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT '申请单号',
  `apply_no_prefix` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '单号前缀（YZ）',
  `apply_status` tinyint(2) UNSIGNED NULL DEFAULT 1 COMMENT '申请状态（字典：oa_seal_apply_status）',
  `node_id` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '当前节点主键',
  `apply_approve_node` tinyint(2) NULL DEFAULT 0 COMMENT '当前节点（0未提交，1审核，2会审，3审批，4已完成）',
  `files_to_unit` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '文件流向单位全称',
  `seal_usage` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '印章用途',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  `seal_company_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '印章所属公司（多个则顿号隔开）',
  `file_name` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用印文件名称（多个则顿号隔开）',
  `seal_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '印章类别（多个则顿号隔开）',
  `file_total` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '用印文件总数（盖章文件数）',
  `stamped_total` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '已盖章文件总数',
  `seal_total` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '落款章总数',
  `seal_on_perforation_total` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '骑缝章总数',
  `attachment_total` smallint(5) UNSIGNED NULL DEFAULT 0 COMMENT '附件数量',
  `approve_index` smallint(5) UNSIGNED NULL DEFAULT 0 COMMENT '版本序号：递增，记录当前是第几次提交（未提交过则为0）',
  `latest_version_flag` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '最新版本标识',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建人',
  `create_code` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '创建人工号',
  `create_department` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建人一级部门',
  `create_dept_code` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '创建人一级部门编号',
  `create_group` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建人二级部门',
  `create_group_code` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '创建人二级部门编号',
  `create_position` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建人职位',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `update_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新人',
  `update_code` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '更新人工号',
  `submit_time` datetime(0) NULL DEFAULT NULL COMMENT '提交时间',
  `cancel_time` datetime(0) NULL DEFAULT NULL COMMENT '撤销时间',
  `approved_time` datetime(0) NULL DEFAULT NULL COMMENT '审批通过时间',
  `deleted_flag` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '删除标记（0未删除，1已删除）',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_apply_status`(`apply_status`) USING BTREE,
  INDEX `index_create_code`(`create_code`) USING BTREE,
  INDEX `index_update_time`(`update_time`) USING BTREE,
  INDEX `index_submit_time`(`submit_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 115 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '印章使用申请记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for oa_remake_seal_authorization
-- ----------------------------
-- DROP TABLE IF EXISTS `oa_remake_seal_authorization`;
CREATE TABLE `oa_remake_seal_authorization`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `auth_type` tinyint(2) NULL DEFAULT 1 COMMENT '权限类型（1-审批人，2-查看员）',
  `emp_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '审批人',
  `emp_code` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '审批人工号',
  `emp_department` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '审批人部门',
  `emp_dept_code` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '审批人部门编号',
  `emp_position` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '审批人职位',
  `primary_department` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '一级部门',
  `primary_dept_code` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '一级部门编号',
  `secondary_department` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '二级部门',
  `secondary_dept_code` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '二级部门编号',
  `start_date` datetime(0) NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '生效日期（默认：1970-01-01 00:00:00）',
  `end_date` datetime(0) NOT NULL DEFAULT '9999-12-31 23:59:59' COMMENT '结束日期（默认：9999-12-31 23:59:59）',
  `approver_status` tinyint(2) NULL DEFAULT 1 COMMENT '授权状态（字典：oa_seal_approver_status）',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建人',
  `create_code` int(11) NULL DEFAULT 0 COMMENT '创建人工号',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `update_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新人',
  `update_code` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '更新人工号',
  `deleted_flag` tinyint(1) NULL DEFAULT 0 COMMENT '删除标记（0未删除，1已删除）',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_approver_code`(`emp_code`, `approver_status`) USING BTREE,
  INDEX `index_status_update_time`(`approver_status`, `update_time`) USING BTREE,
  INDEX `index_primary_dept`(`primary_dept_code`) USING BTREE,
  INDEX `Index_secondary_dept`(`secondary_dept_code`) USING BTREE,
  INDEX `index_start_date`(`start_date`) USING BTREE,
  INDEX `index_end_date`(`end_date`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 135 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '印章权限配置（审批人、查看员）' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for oa_remake_seal_base
-- ----------------------------
-- DROP TABLE IF EXISTS `oa_remake_seal_base`;
CREATE TABLE `oa_remake_seal_base`  (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '编码方式：YYY+MM+DD+3位顺序码，如：20180101001',
  `seal_company_code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '印章所属公司编码',
  `seal_company_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '印章所属公司名称',
  `seal_type` tinyint(2) UNSIGNED NULL DEFAULT 1 COMMENT '印章类别（字典：oa_seal_type）',
  `seal_use_rule` tinyint(2) UNSIGNED NULL DEFAULT 1 COMMENT '盖章规则（字典：oa_seal_use_rule）',
  `seal_status` tinyint(2) UNSIGNED NULL DEFAULT 1 COMMENT '印章状态（字典：oa_seal_seal_status）',
  `manager_count` smallint(5) UNSIGNED NULL DEFAULT 0 COMMENT '管理员总数',
  `start_date` datetime(0) NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '生效日期（默认：1970-01-01 00:00:00）',
  `end_date` datetime(0) NOT NULL DEFAULT '9999-12-31 23:59:59' COMMENT '结束日期（默认：9999-12-31 23:59:59）',
  `province_code` smallint(5) UNSIGNED NULL DEFAULT 0 COMMENT '省份编码',
  `province_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '省份名称',
  `city_code` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '城市编码',
  `city_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '城市名称',
  `county_code` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '区县编码',
  `county_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '区县名称',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '详细地址',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建人',
  `create_code` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '创建人工号',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `update_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新人',
  `update_code` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '更新人工号',
  `deleted_flag` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '删除标记（0未删除，1已删除）',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_start_date`(`start_date`) USING BTREE,
  INDEX `index_end_date`(`end_date`) USING BTREE,
  INDEX `index_status_update_time`(`seal_status`, `update_time`) USING BTREE,
  INDEX `index_company_code`(`seal_company_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '印章主数据' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for oa_remake_seal_download_record
-- ----------------------------
-- DROP TABLE IF EXISTS `oa_remake_seal_download_record`;
CREATE TABLE `oa_remake_seal_download_record`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `apply_id` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '申请主键',
  `apply_no` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT '申请单号',
  `file_id` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '附件主键',
  `watermark_id` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '水印文件主键',
  `file_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '附件名称或者嵌入水印后名称',
  `emp_code` int(10) NULL DEFAULT 0 COMMENT '操作人工号',
  `emp_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作人姓名',
  `operate_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  `deleted_flag` tinyint(1) NULL DEFAULT 0 COMMENT '删除标记(0-未删除，1-已删除)',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_apply_id`(`apply_id`) USING BTREE,
  INDEX `index_file_id`(`file_id`) USING BTREE,
  INDEX `Index_watermark_id`(`watermark_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '记录文件下载的操作信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for oa_remake_seal_manager
-- ----------------------------
-- DROP TABLE IF EXISTS `oa_remake_seal_manager`;
CREATE TABLE `oa_remake_seal_manager`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `seal_id` bigint(20) UNSIGNED NULL DEFAULT 0 COMMENT '印章编号',
  `manager_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '管理员',
  `manager_code` int(11) NULL DEFAULT 0 COMMENT '管理员工号',
  `manager_position` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '管理员职位',
  `manager_department` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '管理员部门',
  `manager_dept_code` int(11) NULL DEFAULT 0 COMMENT '管理员部门编码',
  `start_date` datetime(0) NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '生效日期（默认：1970-01-01 00:00:00）',
  `end_date` datetime(0) NOT NULL DEFAULT '9999-12-31 23:59:59' COMMENT '结束日期（默认：9999-12-31 23:59:59）',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建人',
  `create_code` int(11) NULL DEFAULT 0 COMMENT '创建人工号',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间、删除时间',
  `update_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新人、删除人',
  `update_code` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '更新人工号、删除人工号',
  `deleted_flag` tinyint(1) NULL DEFAULT 0 COMMENT '删除标记（0未删除，1已删除）',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_seal_id`(`seal_id`) USING BTREE,
  INDEX `index_manager_code`(`manager_code`) USING BTREE,
  INDEX `index_start_date`(`start_date`) USING BTREE,
  INDEX `index_end_date`(`end_date`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 87 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '印章管理员' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for oa_remake_seal_usage_record
-- ----------------------------
-- DROP TABLE IF EXISTS `oa_remake_seal_usage_record`;
CREATE TABLE `oa_remake_seal_usage_record`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `apply_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '申请编号',
  `apply_details_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用印信息主键',
  `seal_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '印章编号',
  `seal_use_rule` tinyint(2) UNSIGNED NULL DEFAULT 1 COMMENT '盖章规则（字典：oa_seal_use_rule）',
  `this_stamped_count` int(11) NULL DEFAULT 0 COMMENT '本次盖章份数',
  `this_watermark_flag` tinyint(1) NULL DEFAULT 0 COMMENT '本次确认水印（0无水印，1带水印）',
  `finish_stamp_flag` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '是否完成盖章（0未完成，1已完成）',
  `action_type` tinyint(2) UNSIGNED NOT NULL DEFAULT 1 COMMENT '操作类型（1盖章、2确认、3退回）',
  `stamp_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作人（盖章人、确认人）',
  `stamp_code` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '操作人工号',
  `stamp_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作人时间',
  `deleted_flag` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除标记（0未删除，1已删除）',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_apply_id`(`apply_id`) USING BTREE,
  INDEX `index_apply_details_id`(`apply_details_id`) USING BTREE,
  INDEX `index_seal_id`(`seal_id`) USING BTREE,
  INDEX `index_stamp_code`(`stamp_code`) USING BTREE,
  INDEX `index_stamp_time`(`stamp_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 77 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '印章申请用印信息盖章记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for oa_remake_seal_watermark_record
-- ----------------------------
-- DROP TABLE IF EXISTS `oa_remake_seal_watermark_record`;
CREATE TABLE `oa_remake_seal_watermark_record`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `file_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '附件主键',
  `watermark_file_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '带水印文件名称',
  `watermark_file_size` decimal(13, 2) UNSIGNED NULL DEFAULT 0.00 COMMENT '带水印文件大小',
  `watermark_file_url` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '带水印文件地址',
  `watermark_text` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '明文水印内容',
  `watermark_font` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '明文水印字体',
  `watermark_font_size` smallint(6) NULL DEFAULT 12 COMMENT '明文水印字号',
  `add_watermark_time` datetime(0) NULL DEFAULT NULL COMMENT '嵌入水印时间',
  `add_watermark_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '嵌入水印人员姓名',
  `add_watermark_code` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '嵌入水印人员工号',
  `deleted_flag` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '删除标记（0未删除，1已删除）',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_file_id`(`file_id`) USING BTREE,
  INDEX `index_add_watermark_time`(`add_watermark_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 79 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '印章申请附件嵌入水印记录' ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
